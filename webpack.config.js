const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('plugins_js', './assets/front/js/plugins.js')
    .addEntry('theme_js', './assets/front/js/theme.js')
    .addStyleEntry('plugins_css', './assets/front/css/plugins.css')
    .addStyleEntry('style_css', './assets/front/css/style.css')
    .addStyleEntry('color_css', './assets/front/css/colors/red.css')
    .addStyleEntry('thicccboi_font', './assets/front/css/fonts/thicccboi.css')

    .addEntry('subscription_js', './assets/front/cgt/subscription.js')

    /*
     * Admin template assets & plugins
     */
    .addEntry('bo_main_js', './assets/bo/js/main.js')
    .addEntry('bo_custom_js', './assets/bo/js/custom.js')
    .addEntry('bo_dashboard_js', './assets/bo/js/pages/dashboard.js')
    .addStyleEntry('bo_main_css', './assets/bo/css/main.css')
    .addStyleEntry('bo_horizontal_menu_css', './assets/bo/css/horizontal-menu/horizontal-menu.css')
    .addStyleEntry('bo_custom_css', './assets/bo/css/custom.css')

    .addStyleEntry('bo_bootstrap_css', './assets/bo/plugins/bootstrap/css/bootstrap.min.css')
    .addStyleEntry('bo_perfect_scrollbar_css', './assets/bo/plugins/perfectscroll/perfect-scrollbar.css')
    .addStyleEntry('bo_pace_css', './assets/bo/plugins/pace/pace.css')
    .addEntry('bo_jquery_js', './assets/bo/plugins/jquery/jquery-3.5.1.min.js')
    .addEntry('bo_bootstrap_js', './assets/bo/plugins/bootstrap/js/bootstrap.min.js')
    .addEntry('bo_perfect_scrollbar_js', './assets/bo/plugins/perfectscroll/perfect-scrollbar.min.js')
    .addEntry('bo_pace_js', './assets/bo/plugins/pace/pace.min.js')
    .addEntry('bo_apexcharts_js', './assets/bo/plugins/apexcharts/apexcharts.min.js')

    .addEntry('bo_popper_js', './assets/bo/plugins/bootstrap/js/popper.min.js')
    .addEntry('bo_highlight_js', './assets/bo/plugins/highlight/highlight.pack.js')
    .addEntry('bo_datatables_js', './assets/bo/plugins/datatables/datatables.min.js')
    .addEntry('bo_datatables_page_js', './assets/bo/js/pages/datatables.js')
    .addStyleEntry('bo_datatables_css', './assets/bo/plugins/datatables/datatables.min.css')
    .addStyleEntry('bo_highlight_css', './assets/bo/plugins/highlight/styles/github-gist.css')

    .addEntry('bo_delete_from_list_js', './assets/bo/cgt/js/bo-delete-from-list.js')

    .copyFiles({
      from: './assets/front/img',
      to: 'front/images/[path][name].[ext]'
    })

    .copyFiles({
      from: './assets/front/js/plugins',
      to: 'front/js/plugins/[path][name].[ext]'
    })

    // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    .enableStimulusBridge('./assets/controllers.json')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // enables Sass/SCSS support
    .enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use React
    //.enableReactPreset()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    // .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
