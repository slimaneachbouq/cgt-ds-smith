

    function checkLabelDownloadCookie(){
        const cookie =  Cookies.get('cgt_subscription');
        if( cookie ){
            Cookies.remove('cgt_subscription'); //remove cookie
            location.reload(); //reload page
        }
    }

    function monitorLabelDownloadCookie(){
        // check if the cookie exists every 500ms
        setInterval( checkLabelDownloadCookie, 500 );
    }

    monitorLabelDownloadCookie();
