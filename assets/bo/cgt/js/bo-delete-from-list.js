import Swal from 'sweetalert2';


let deleteLinks = document.getElementsByClassName('delete-btn');
for (let i = 0; i < deleteLinks.length; i++) {
    deleteLinks[i].addEventListener('click',deleteArticle,false);
}

function deleteArticle() {
    let deletePath = this.dataset.deletePath;
    Swal.fire({
        title: 'Es-tu sûr?',
        text: "Vous ne pourrez pas revenir en arrière !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, supprimez-le !',
        cancelButtonText: 'Annuler'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: deletePath,
                method: 'DELETE',
            }).done(function() {
                window.location.reload(true);
            });
        }
    })
}


