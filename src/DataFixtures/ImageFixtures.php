<?php

namespace App\DataFixtures;

use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class ImageFixtures extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['images'];
    }

    public function load(ObjectManager $manager)
    {
        $image = new Image();
        $image->setIdentifier('pp');
        $manager->persist($image);

        $image2 = new Image();
        $image2->setIdentifier('cc');
        $manager->persist($image2);

        $manager->flush();
    }
}
