<?php

namespace App\DataFixtures;

use App\Entity\SocialActivities;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class SocialActivitiesFixtures extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['socialPage'];
    }

    public function load(ObjectManager $manager): void
    {
        $socialActivities = new SocialActivities();
        $socialActivities->setIdentifier('socialPage');
        $socialActivities->setContent(null);
        $manager->persist($socialActivities);

        $manager->flush();
    }
}
