<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['category'];
    }

    public function load(ObjectManager $manager): void
    {
        $category1 = new Category();
        $category1->setName('Publications CGT');
        $category1->setType('article');
        $manager->persist($category1);

        $category2 = new Category();
        $category2->setName('La FILPAC');
        $category2->setType('article');
        $manager->persist($category2);

        $category3 = new Category();
        $category3->setName('Questions CGT au CS2E');
        $category3->setType('document');
        $manager->persist($category3);

        $category4 = new Category();
        $category4->setName('Les Bulletins CGT');
        $category4->setType('document');
        $manager->persist($category4);

        $category5 = new Category();
        $category5->setName('La pp');
        $category5->setType('document');
        $manager->persist($category5);

        $category6 = new Category();
        $category6->setName('Les Conventions Collectives');
        $category6->setType('document');
        $manager->persist($category6);

        $manager->flush();
    }
}
