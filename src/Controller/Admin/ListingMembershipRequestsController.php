<?php

namespace App\Controller\Admin;

use App\Repository\MembershipRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListingMembershipRequestsController extends AbstractController
{
    /**
     * @Route("/admin/list-subscription-requests", name="listing_subscription_requests")
     */
    public function __invoke(Request $request, MembershipRequestRepository $membershipRequestRepository)
    {
        $subscriptions = $membershipRequestRepository->findAll();
        return $this->render('admin/subscription/list.html.twig', [
            'subscriptions' => $subscriptions,
        ]);
    }
}
