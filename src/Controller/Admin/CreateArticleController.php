<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Form\ArticleFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Gedmo\Sluggable\Util\Urlizer;

class CreateArticleController extends AbstractController
{
    /**
     * @Route("/admin/create-article", name="create_article")
     */
    public function __invoke(Request $request, EntityManagerInterface $entityManager)
    {
        $article = new Article();
        $form = $this->createForm(ArticleFormType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFile = $form['imageFile']->getData();
            $docFile = $form['file']->getData();

            /** Upload service image */
            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$imageFile->guessExtension();
                $imageFile->move(
                    $this->getParameter('service_images_dir'),
                    $newFilename
                );
                $article->setImageName($newFilename);
            }

            /** Upload service doc */
            if ($docFile) {
                $originalFilename = pathinfo($docFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$docFile->guessExtension();
                $docFile->move(
                    $this->getParameter('service_docs_dir'),
                    $newFilename
                );
                $article->setFileName($newFilename);
            }

            $entityManager->persist($article);
            $entityManager->flush();
            $this->addFlash('success', 'Article créé avec succès !');
            return $this->redirectToRoute('listing_articles');
        }
        return $this->render('admin/article/create.html.twig', [
            'articleForm' => $form->createView(),
        ]);
    }
}
