<?php

namespace App\Controller\Admin;

use App\Entity\Document;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DeleteDocumentController extends AbstractController
{
    /**
     * @Route("/admin/delete-document/{id}", name="delete_document", methods={"DELETE"})
     */
    public function __invoke(Document $document, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        try {
            $entityManager->remove($document);
            $entityManager->flush();
        } catch (\Exception $exception) {
            $logger->error($exception->getMessage());
        }

        return $this->redirectToRoute('listing_documents');
    }
}
