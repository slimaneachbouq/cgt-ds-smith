<?php

namespace App\Controller\Admin;

use App\Entity\Document;
use App\Form\DocumentFormType;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateDocumentController extends AbstractController
{
    /**
     * @Route("/admin/create-document", name="create_document")
     */
    public function __invoke(Request $request, EntityManagerInterface $entityManager)
    {
        $document = new Document();
        $form = $this->createForm(DocumentFormType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $docFile = $form['file']->getData();

            /** Upload service image */
            if ($docFile) {
                $originalFilename = pathinfo($docFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$docFile->guessExtension();
                $docFile->move(
                    $this->getParameter('document_files_dir'),
                    $newFilename
                );
                $document->setFileName($newFilename);
            }

            $entityManager->persist($document);
            $entityManager->flush();
            $this->addFlash('success', 'Document créé avec succès !');
            return $this->redirectToRoute('listing_documents');
        }

        return $this->render('admin/document/create.html.twig', [
            'documentForm' => $form->createView(),
        ]);
    }
}
