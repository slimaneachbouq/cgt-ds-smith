<?php

namespace App\Controller\Admin;

use App\Entity\SocialActivities;
use App\Form\SocialActivitiesFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditSocialActivitiesPageController extends AbstractController
{
    /**
     * @Route("/admin/edit-docial-activities-page/{identifier}", name="edit_social_activities_page", defaults={"identifier" = "socialPage"})
     * @ParamConverter("socialActivities", options={"mapping": {"identifier": "identifier"}})
     */
    public function __invoke(Request $request, SocialActivities $socialActivities, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(SocialActivitiesFormType::class, $socialActivities);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $socialActivities = $form->getData();
            $entityManager->persist($socialActivities);
            $entityManager->flush();

            return $this->redirectToRoute('social_activities_page');
        }

        return $this->render('admin/socialActivitiesPage/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
