<?php

namespace App\Controller\Admin;

use App\Form\ResetPasswordFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordController extends AbstractController
{
    /**
     * @Route("/admin/change-password", name="change_password")
     */
    public function __invoke(
        Request $request,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $user = $this->getUser();

        $form = $this->createForm(ResetPasswordFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $oldPassword = $request->request->get('reset_password_form')['oldPassword'];

            if ($passwordEncoder->isPasswordValid($user, $oldPassword)) {
                $newPassword = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($newPassword);
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash('success', 'Votre mot de passe à bien été changé !');

                return $this->redirectToRoute('change_password');
            } else {
                $this->addFlash('danger', 'Ancien mot de passe incorrect !');
            }
        }

        return $this->render('admin/reset_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
