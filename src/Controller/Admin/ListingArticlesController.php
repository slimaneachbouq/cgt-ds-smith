<?php

namespace App\Controller\Admin;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListingArticlesController extends AbstractController
{
    /**
     * @Route("/admin/list-articles", name="listing_articles")
     */
    public function __invoke(Request $request, ArticleRepository $articleRepository)
    {
        $articles = $articleRepository->findAll();
        return $this->render('admin/article/list.html.twig', [
            'articles' => $articles,
        ]);
    }
}
