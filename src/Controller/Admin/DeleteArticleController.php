<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteArticleController extends AbstractController
{
    /**
     * @Route("/admin/delete-article/{id}", name="delete_article", methods={"DELETE"})
     */
    public function __invoke(Article $article, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        try {
            $entityManager->remove($article);
            $entityManager->flush();
        } catch (\Exception $exception) {
            $logger->error($exception->getMessage());
        }

        return $this->redirectToRoute('listing_articles');
    }
}
