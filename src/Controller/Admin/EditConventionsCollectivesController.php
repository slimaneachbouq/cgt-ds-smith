<?php

namespace App\Controller\Admin;

use App\Entity\Image;
use App\Form\ImageFormType;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EditConventionsCollectivesController extends AbstractController
{
    /**
     * @Route("/admin/edit-cc-image/{identifier}", name="edit_cc_image", defaults={"identifier" = "cc"})
     * @ParamConverter("image", options={"mapping": {"identifier": "identifier"}})
     */
    public function __invoke(Request $request, Image $image, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(ImageFormType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFile = $form['imageFile']->getData();
            /** Upload service image */
            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$imageFile->guessExtension();
                $imageFile->move(
                    $this->getParameter('images_dir'),
                    $newFilename
                );
                $image->setImageName($newFilename);
            }

            $entityManager->persist($image);
            $entityManager->flush();

            return $this->redirectToRoute('cc_page');
        }

        return $this->render('admin/image/edit_cc_image.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
