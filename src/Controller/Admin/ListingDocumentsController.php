<?php

namespace App\Controller\Admin;

use App\Repository\DocumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListingDocumentsController extends AbstractController
{
    /**
     * @Route("/admin/list-documents", name="listing_documents")
     */
    public function __invoke(Request $request, DocumentRepository $documentRepository)
    {
        $documents = $documentRepository->findAll();
        return $this->render('admin/document/list.html.twig', [
            'documents' => $documents,
        ]);
    }
}
