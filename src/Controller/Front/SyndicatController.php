<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SyndicatController extends AbstractController
{
    /**
     * @Route("/le-syndicat-cgt-ds-smith-st-just", name="syndicat_page")
     */
    public function __invoke()
    {
        return $this->render('front/syndicat.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }
}
