<?php

namespace App\Controller\Front;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FilpacController extends AbstractController
{
    /**
     * @Route("/filpac", name="filpac")
     */
    public function __invoke(ArticleRepository $articleRepository): Response
    {
        $cgtPublications = $articleRepository
            ->createQueryBuilder('a')
            ->join('a.category', 'c')
            ->where('c.name = :name')
            ->setParameter('name', 'La FILPAC')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('front/filpac.html.twig', [
            'articles' => $cgtPublications,
        ]);
    }
}
