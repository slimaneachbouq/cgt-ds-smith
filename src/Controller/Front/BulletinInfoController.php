<?php

namespace App\Controller\Front;

use App\Repository\DocumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BulletinInfoController extends AbstractController
{
    /**
     * @Route("/bulletin-info", name="bulletin_info")
     */
    public function __invoke(DocumentRepository $documentRepository): Response
    {
        $documents = $documentRepository
            ->createQueryBuilder('d')
            ->join('d.category', 'c')
            ->where('c.name = :name')
            ->setParameter('name', 'Les Bulletins CGT')
            ->orderBy('d.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('front/bulletin_info.html.twig', [
            'documents' => $documents,
        ]);
    }
}
