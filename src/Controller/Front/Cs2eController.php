<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Cs2eController extends AbstractController
{
    /**
     * @Route("/cs2e", name="cs2e")
     */
    public function __invoke(): Response
    {
        return $this->render('front/cs2e.html.twig', [
            'controller_name' => 'FilpacController',
        ]);
    }
}
