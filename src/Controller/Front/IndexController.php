<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function __invoke()
    {
      return $this->render('front/index.html.twig', [
        'controller_name' => 'IndexController',
      ]);
    }
}
