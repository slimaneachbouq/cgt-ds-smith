<?php

namespace App\Controller\Front;

use App\Repository\DocumentRepository;
use App\Repository\ImageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PpController extends AbstractController
{
    /**
     * @Route("/la-prime-de-performance", name="pp_page")
     */
    public function __invoke(DocumentRepository $documentRepository, ImageRepository $imageRepository): Response
    {
        $pp = $documentRepository
            ->createQueryBuilder('d')
            ->join('d.category', 'c')
            ->where('c.name = :name')
            ->setParameter('name', 'La pp')
            ->orderBy('d.id', 'DESC')
            ->getQuery()
            ->getResult();

        $image = $imageRepository->findOneBy(['identifier' => 'pp']);

        return $this->render('front/pp.html.twig', [
            'documents' => $pp,
            'image' => $image
        ]);
    }
}
