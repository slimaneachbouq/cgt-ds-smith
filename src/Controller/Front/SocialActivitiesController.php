<?php

namespace App\Controller\Front;

use App\Repository\SocialActivitiesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SocialActivitiesController extends AbstractController
{
    /**
     * @Route("/activites-sociales", name="social_activities_page")
     */
    public function __invoke(SocialActivitiesRepository $activitiesRepository)
    {
        return $this->render('front/social_activities.html.twig', [
            'page' => $activitiesRepository->findOneBy(['identifier' => 'socialPage'])
        ]);
    }
}
