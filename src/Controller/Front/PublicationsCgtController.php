<?php

namespace App\Controller\Front;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PublicationsCgtController extends AbstractController
{
    /**
     * @Route("/publications-cgt", name="publications_cgt")
     */
    public function __invoke(ArticleRepository $articleRepository): Response
    {
        $cgtPublications = $articleRepository
            ->createQueryBuilder('a')
            ->join('a.category', 'c')
            ->where('c.name = :name')
            ->setParameter('name', 'Publications CGT')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('front/publications_cgt.html.twig', [
            'articles' => $cgtPublications,
        ]);
    }
}
