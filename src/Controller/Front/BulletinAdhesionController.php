<?php

namespace App\Controller\Front;

use App\Entity\MembershipRequest;
use App\Form\MembershipRequestFormType;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BulletinAdhesionController extends AbstractController
{
    /**
     * @Route("/bulletin-adhesion", name="generate_bulletin_adhesion")
     */
    public function __invoke(Request $request, EntityManagerInterface $entityManager)
    {
        $memberShipRequest = new MembershipRequest();
        $form = $this->createForm(MembershipRequestFormType::class, $memberShipRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $memberShipRequest->setFileName($this->generateMembershipRequestPdf($memberShipRequest));
            $entityManager->persist($memberShipRequest);
            $entityManager->flush();

            return $this->redirectToRoute('generate_bulletin_adhesion');
        }

        return $this->render('front/bulletin_adhesion.html.twig', [
            'memberShipRequestForm' => $form->createView(),
        ]);
    }

    /**
     * @param MembershipRequest $membershipRequest
     * @return string
     */
    private function generateMembershipRequestPdf(MembershipRequest $membershipRequest): String
    {
        $fileName = Urlizer::urlize('subscription').'-'.uniqid().'.pdf';
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $dompdf = new Dompdf($pdfOptions);
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('front/bulletin_adhesion_pdf.html.twig', [
            'last_name' => $membershipRequest->getLastName(),
            'first_name' => $membershipRequest->getFirstName(),
            'birthday' => $membershipRequest->getBirthday()->format('Y-m-d'),
            'professional_category' => $membershipRequest->getProfessionalCategory(),
            'address' => $membershipRequest->getAddress(),
            'cp' => $membershipRequest->getCp(),
            'city' => $membershipRequest->getCity(),
            'phone' => $membershipRequest->getPhone(),
            'email'=> $membershipRequest->getEmail(),
            'subscription_date' => $membershipRequest->getCreatedAt()->format('Y-m-d')
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        file_put_contents($this->getParameter('pdf_subscription_dir') . '/' .$fileName, $dompdf->output());

        $this->addFlash('success', 'Votre demande a été bien enregistrer !');
        setcookie('cgt_subscription', 'download complete', time() + 86400, "/");

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);

        return $fileName;
    }
}
