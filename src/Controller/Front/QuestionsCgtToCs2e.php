<?php

namespace App\Controller\Front;

use App\Repository\DocumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuestionsCgtToCs2e extends AbstractController
{
    /**
     * @Route("/questions_cgt_to_cs2e", name="questions_cgt_to_cs2e")
     */
    public function __invoke(DocumentRepository $documentRepository): Response
    {
        $documents = $documentRepository
            ->createQueryBuilder('d')
            ->join('d.category', 'c')
            ->where('c.name = :name')
            ->setParameter('name', 'Questions CGT au CS2E')
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult();
        return $this->render('front/questions_cgt_to_cs2e.html.twig', [
            'documents' => $documents
        ]);
    }
}
