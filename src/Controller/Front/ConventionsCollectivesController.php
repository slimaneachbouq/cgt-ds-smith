<?php

namespace App\Controller\Front;

use App\Repository\DocumentRepository;
use App\Repository\ImageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConventionsCollectivesController extends AbstractController
{
    /**
     * @Route("/les-conventions-collectives", name="cc_page")
     */
    public function __invoke(DocumentRepository $documentRepository, ImageRepository $imageRepository): Response
    {
        $pp = $documentRepository
            ->createQueryBuilder('d')
            ->join('d.category', 'c')
            ->where('c.name = :name')
            ->setParameter('name', 'Les Conventions Collectives')
            ->orderBy('d.id', 'DESC')
            ->getQuery()
            ->getResult();

        $image = $imageRepository->findOneBy(['identifier' => 'cc']);

        return $this->render('front/conventions_collectives.html.twig', [
            'documents' => $pp,
            'image' => $image
        ]);
    }
}
