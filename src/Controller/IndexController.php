<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\DocumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_dashoard")
     */
    public function __invoke(ArticleRepository $articleRepository, DocumentRepository $documentRepository): Response
    {
        $articlesNbr = $articleRepository->count([]);
        $documentsNbr = $documentRepository->count([]);

        return $this->render('admin/index.html.twig', [
            'articles_nbr' => $articlesNbr,
            'documents_nbr' => $documentsNbr,
        ]);
    }
}
