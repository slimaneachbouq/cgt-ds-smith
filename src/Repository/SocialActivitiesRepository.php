<?php

namespace App\Repository;

use App\Entity\SocialActivities;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SocialActivities|null find($id, $lockMode = null, $lockVersion = null)
 * @method SocialActivities|null findOneBy(array $criteria, array $orderBy = null)
 * @method SocialActivities[]    findAll()
 * @method SocialActivities[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocialActivitiesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SocialActivities::class);
    }

    // /**
    //  * @return SocialActivities[] Returns an array of SocialActivities objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SocialActivities
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
