<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title',  TextType::class, [
              'label' => false,
              'attr' => [
                'class' => 'form-control'
              ]
            ])
            ->add('imageFile', FileType::class, [
              'label' => false,
              'mapped' => false,
              'attr' => [
                'class' => 'form-control'
              ]
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'label' => false,
                'attr' => [
                    'class' => 'form-control'
                ],
                'query_builder' => function(EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('category')
                        ->where('category.type = :type')
                        ->setParameter('type', 'article');
                },
            ])
            ->add('description', TextareaType::class, [
              'label' => false,
              'attr' => [
                'class' => 'form-control'
              ]
            ])
            ->add('file', FileType::class, [
              'label' => false,
              'mapped' => false,
              'attr' => [
                'class' => 'form-control'
              ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
