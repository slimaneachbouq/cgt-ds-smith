########################
## AZULANCE MAKEFILE <3
########################

APP_ENV ?= dev
APP_CONTAINER ?= cgt_app
APP_PROJECT_DIR ?= /var/www/html/

DOCKER ?= docker
EXEC := $(DOCKER) exec -w $(APP_PROJECT_DIR) -u $(id -u ${USER}):$(id -g ${USER}) -ti $(APP_CONTAINER)
EXEC_BASH := $(EXEC) bash -c
EXEC_SSH := $(EXEC) bash

YARN ?= $(EXEC) yarn
COMPOSER ?= $(EXEC) composer
ASSETS_ENV ?= dev

build:
	docker-compose build

start:
	docker-compose up -d

ssh:
	$(EXEC_SSH)

install:
	$(EXEC_BASH) "composer install"

update:
	$(EXEC_BASH) "composer update"

node_modules:
	$(YARN) install

watch:
	$(YARN) "run watch"

cs:
	$(EXEC_BASH) "./vendor/bin/phpcs"

csfix:
	$(EXEC_BASH) "./vendor/bin/phpcbf"



